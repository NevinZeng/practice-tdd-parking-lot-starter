package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {

    @Test
    void should_return_a_ticket_when_park_car_given_a_car_and_parkingLot() {
        // given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();

        // when
        Ticket ticket = parkingLot.park(car);

        // then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_car_when_fetch_given_a_ticket_and_parkingLot_parked_car() {
        // given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket = parkingLot.park(car);

        // when
        Car fetchedCar = parkingLot.fetch(ticket);

        // then
        Assertions.assertNotNull(fetchedCar);
    }

    @Test
    void should_return_right_car_with_each_ticket_when_fetch_car_twice_given_a_ticket_and_parkingLot_with_two_parked_car() {
        // given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);

        // when
        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);

        // then
        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);

    }

    @Test
    void should_throw_unrecognized_exception_when_fetch_given_a_wrong_ticket_and_parkingLot() {
        // given
        Ticket ticket = new Ticket();
        ParkingLot parkingLot = new ParkingLot();

        // when
        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLot.fetch(ticket));

        // then
        Assertions.assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_unrecognized_exception_when_fetch_given_a_used_ticket_and_parkingLot() {
        // given
        Ticket ticket = new Ticket();
        ParkingLot parkingLot = new ParkingLot();

        // when
        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingLot.fetch(ticket));

        // then
        Assertions.assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_noAvailablePosition_exception_when_park_car_given_a_car_and_parkingLot_without_position() {
        // given
        ParkingLot parkingLot = new ParkingLot(2);
        Car car;
        for (int index = 0; index < 2; index++) {
            car = new Car();
            parkingLot.park(car);
        }
        Car currentCar = new Car();

        // when
        NoAvailablePositionException noAvailablePositionException =
                Assertions.assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(currentCar));

        // then
        Assertions.assertEquals("No available position.", noAvailablePositionException.getMessage());

    }

}
