package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ParkingManager {

    private List<ParkingBoy> managementList = new ArrayList<>();

    private ParkingStrategy parkingStrategy;

    public ParkingManager() {

    }

    public ParkingManager(ParkingStrategy parkingStrategy) {
        this.parkingStrategy = parkingStrategy;
    }

    public List<ParkingBoy> addParkingBoyToManagementList(ParkingBoy parkingBoy) {
        managementList.add(parkingBoy);
        return managementList;
    }

    public Ticket park(Car car) {
        if (parkingStrategy != null) {
            return parkingStrategy.park(car);
        }
        return null;
    }

    public Car fetch(Ticket ticket) {
        if (parkingStrategy != null) {
            return parkingStrategy.fetch(ticket);
        }
        return null;
    }
}
