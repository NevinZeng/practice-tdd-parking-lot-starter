package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ParkingManagerTest {

    @Test
    void should_add_parkingBoy_to_management_list_when_add_parkingBoy_given_a_manager_and_a_standard_and_a_smart_parkingBoy() {
        ParkingManager parkingManager = new ParkingManager();
        ParkingBoy standardParkingBoy = new ParkingBoy(List.of(new ParkingLot()));
        ParkingBoy smartParkingBoy = new ParkingBoy(List.of(new ParkingLot()));
        parkingManager.addParkingBoyToManagementList(standardParkingBoy);
        List<ParkingBoy> parkingBoyList = parkingManager.addParkingBoyToManagementList(smartParkingBoy);

        Assertions.assertEquals(standardParkingBoy, parkingBoyList.get(0));
        Assertions.assertEquals(smartParkingBoy, parkingBoyList.get(1));
    }
}
