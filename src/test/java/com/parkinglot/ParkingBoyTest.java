package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ParkingBoyTest {

    @Test
    void should_parked_to_first_parkingLot_when_park_car_given_a_car_and_super_smart_parkingBoy_and_two_parkingLot_with_capacity_3_10_and_empty_position_2_5() {
        // given
        List<ParkingLot> parkingLotList = List.of(new ParkingLot(3), new ParkingLot(10));
        SuperSmartParkingBoyStrategy parkingBoy = new SuperSmartParkingBoyStrategy(parkingLotList);
        parkingBoy.getParkingLotList().get(0).park(new Car());
        for (int index = 0; index < 5; index++) {
            parkingBoy.getParkingLotList().get(1).park(new Car());
        }
        Car car = new Car();

        // when
        Ticket ticket = parkingBoy.park(car);
        ParkingLot firstParkingLot = parkingBoy.getParkingLotList().get(0);

        // then
        assertEquals(car, firstParkingLot.fetch(ticket));
    }

    @Test
    void should_parked_to_second_parkingLot_when_park_car_given_a_car_and_smart_parkingBoy_and_two_parkingLot_with_capacity_3_10_and_empty_position_2_5() {
        // given
        List<ParkingLot> parkingLotList = List.of(new ParkingLot(3), new ParkingLot(10));
        SmartParkingBoyStrategy parkingBoy = new SmartParkingBoyStrategy(parkingLotList);
        parkingBoy.getParkingLotList().get(0).park(new Car());
        for (int index = 0; index < 5; index++) {
            parkingBoy.getParkingLotList().get(1).park(new Car());
        }
        Car car = new Car();

        // when
        Ticket ticket = parkingBoy.park(car);
        ParkingLot firstParkingLot = parkingBoy.getParkingLotList().get(1);

        // then
        assertEquals(car, firstParkingLot.fetch(ticket));
    }

    @Test
    void should_parked_to_first_parkingLot_when_park_car_given_a_car_and_smart_parkingBoy_and_two_parkingLot_with_position_1_5() {
        // given
        List<ParkingLot> parkingLotList = List.of(new ParkingLot(1), new ParkingLot(10));
        StandardParkingBoyStrategy parkingBoy = new StandardParkingBoyStrategy(parkingLotList);
        Car car = new Car();

        // when
        Ticket ticket = parkingBoy.park(car);
        ParkingLot firstParkingLot = parkingBoy.getParkingLotList().get(0);

        // then
        assertEquals(car, firstParkingLot.fetch(ticket));
    }

    @Test
    void should_parked_to_first_parkingLot_when_park_car_given_a_car_and_smart_parkingBoy_and_a_full_and_a_available_position_parkingLot() {
        // given
        Car car = new Car();
        List<ParkingLot> parkingLotList = List.of(new ParkingLot(1), new ParkingLot(2));
        SuperSmartParkingBoyStrategy parkingBoy = new SuperSmartParkingBoyStrategy(parkingLotList);
        parkingBoy.park(car);

        Car car2 = new Car();
        // when
        Ticket ticket = parkingBoy.park(car2);
        ParkingLot firstParkingLot = parkingBoy.getParkingLotList().get(0);

        // then
        assertEquals(car2, firstParkingLot.fetch(ticket));
    }

    @Test
    void should_return_right_car_with_each_ticket_when_fetch_car_twice_given_two_ticket_and_parkingBoy_and_two_parkingLot_all_with_a_parked_car() {
        // given
        Car car1 = new Car();
        Car car2 = new Car();
        List<ParkingLot> parkingLotList = List.of(new ParkingLot(1), new ParkingLot(2));
        SuperSmartParkingBoyStrategy parkingBoy = new SuperSmartParkingBoyStrategy(parkingLotList);
        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);

        // when
        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        Car fetchedCar2 = parkingBoy.fetch(ticket2);

        // then
        assertEquals(car1, fetchedCar1);
        assertEquals(car2, fetchedCar2);
    }

    @Test
    void should_throw_unrecognized_exception_when_fetch_given_a_wrong_ticket_and_parkingBoy_and_two_parkingLot() {
        // given
        Ticket ticket = new Ticket();
        SuperSmartParkingBoyStrategy parkingBoy = new SuperSmartParkingBoyStrategy(List.of(new ParkingLot(), new ParkingLot()));
        // when
        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingBoy.fetch(ticket));

        // then
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_unrecognized_exception_when_fetch_given_a_used_ticket_and_parkingBoy_and_two_parkingLot() {
        // given
        Ticket ticket = new Ticket();
        SuperSmartParkingBoyStrategy parkingBoy = new SuperSmartParkingBoyStrategy(List.of(new ParkingLot(), new ParkingLot()));
        // when
        UnrecognizedTicketException unrecognizedTicketException =
                Assertions.assertThrows(UnrecognizedTicketException.class, () -> parkingBoy.fetch(ticket));

        // then
        assertEquals("Unrecognized parking ticket.", unrecognizedTicketException.getMessage());
    }

    @Test
    void should_throw_noAvailablePosition_exception_when_park_car_given_a_car_and_two_parkingLot_without_position() {
        // given
        SuperSmartParkingBoyStrategy parkingBoy = new SuperSmartParkingBoyStrategy(List.of(new ParkingLot(1), new ParkingLot(1)));
        Car car1 = new Car();
        Car car2 = new Car();
        parkingBoy.park(car1);
        parkingBoy.park(car2);
        Car currentCar = new Car();

        // when
        NoAvailablePositionException noAvailablePositionException =
                Assertions.assertThrows(NoAvailablePositionException.class, () -> parkingBoy.park(currentCar));

        // then
        assertEquals("No available position.", noAvailablePositionException.getMessage());
    }

}
