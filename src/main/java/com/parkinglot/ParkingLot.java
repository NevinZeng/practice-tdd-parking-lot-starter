package com.parkinglot;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class ParkingLot {

    private final Integer position;

    public ParkingLot(){
        this.position = 10;
    }

    public ParkingLot(Integer position){
        this.position = position;
    }

    private final Map<Ticket, Car> ticketCarMap = new HashMap<>();

    public Ticket park(Car car) {
        if (ticketCarMap.size() >= position){
            throw new NoAvailablePositionException();
        }
        Ticket ticket = new Ticket();
        ticketCarMap.put(ticket, car);
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        if (!ticketCarMap.containsKey(ticket)){
            throw new UnrecognizedTicketException();
        }
        return ticketCarMap.remove(ticket);
    }

    public Boolean isFull(){
        return ticketCarMap.size() >= position;
    }

    public Boolean hasTicket(Ticket ticket){
        return ticketCarMap.containsKey(ticket);
    }

    public Integer getEmptyPositionNumber(){
        return position - ticketCarMap.size();
    }

    public Double  getAvailablePositionRate(){
        return new BigDecimal(getEmptyPositionNumber()).doubleValue() / position;
    }
}
