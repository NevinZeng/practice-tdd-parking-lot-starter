package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SmartParkingBoyStrategy extends ParkingBoy implements ParkingStrategy {

    public SmartParkingBoyStrategy(List<ParkingLot> parkingLotList) {
        super(parkingLotList);
    }

    @Override
    public Ticket park(Car car) {
        ParkingLot maxEmptyParkingLot =
                getParkingLotList().stream()
                        .max(Comparator.comparing(ParkingLot::getEmptyPositionNumber))
                        .orElseThrow(NoAvailablePositionException::new);

        return maxEmptyParkingLot.park(car);
    }

}
