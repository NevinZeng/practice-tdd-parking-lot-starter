package com.parkinglot;

import java.util.List;

public class StandardParkingBoyStrategy extends ParkingBoy implements ParkingStrategy {

    public StandardParkingBoyStrategy(List<ParkingLot> parkingLotList) {
        super(parkingLotList);
    }

    @Override
    public Ticket park(Car car) {
        for (ParkingLot parkingLot : getParkingLotList()) {
            if (!parkingLot.isFull()){
                return parkingLot.park(car);
            }
        }
        throw new NoAvailablePositionException();
    }
}
