package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SuperSmartParkingBoyStrategy extends ParkingBoy implements ParkingStrategy {

    public SuperSmartParkingBoyStrategy(List<ParkingLot> parkingLotList) {
        super(parkingLotList);
    }

    @Override
    public Ticket park(Car car) {
        ParkingLot maxEmptyParkingLot =
                getParkingLotList()
                        .stream()
                        .max(Comparator.comparing(ParkingLot::getAvailablePositionRate)
                                .thenComparing(ParkingLot::getEmptyPositionNumber))
                        .orElseThrow(NoAvailablePositionException::new);

        return maxEmptyParkingLot.park(car);
    }
}
