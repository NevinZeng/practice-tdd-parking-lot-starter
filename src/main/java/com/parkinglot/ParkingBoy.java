package com.parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ParkingBoy {


    private final List<ParkingLot> parkingLotList = new ArrayList<>();

    public ParkingBoy(List<ParkingLot> parkingLotList){
        this.parkingLotList.addAll(parkingLotList);
    }

    public Car fetch(Ticket ticket) {
        return getParkingLotList()
                .stream()
                .filter(parkingLot -> parkingLot.hasTicket(ticket))
                .findFirst()
                .map(parkingLot -> parkingLot.fetch(ticket))
                .orElseThrow(UnrecognizedTicketException::new);
    }

    public List<ParkingLot> getParkingLotList() {
        return parkingLotList;
    }
}
